# Purely Functional Data Structures Practise

following code are practise during my reading of \<Purely Functional Data Structures\> 
by Chris Okasaki.

this practise includes data structures introduced in the book as well as exercises.

sample code in the book is in SML, while my code is in Haskell. 
even though there is Haskell sample code in appendix, but I didn't look too much into it.

well actually I read a little bit, but it lacks of concern to compiler config so I actually
don't think that code will compile without any modification.

## About Complexity

in lazy language like Haskell, one shall not use worst-case complexity to describe its running
result, but use amortized complexity instead. worst-case complexity doesn't make good sense to
describe lazy behavior.

but I do here just for matching what the book says.

actually I think data structures are different, since there are pattern matchings everywhere
so there shouldn't be a overwhelming number of thunks getting stacked. 

### Laziness

there are implementations that are aware of laziness in Haskell. those locate in `src/Lazy` folder.

the complexity showed in that folder and its subfolders shall express the true complexity in Haskell.
