{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Lazy.Heap.PairingHeap(PHeap) where

import Heap.Heap

data PHeap a = Empty | PHeap{ node :: a
                            , oddh :: PHeap a
                            , susp :: PHeap a
                            } deriving Show

-- | here the trick is to use laziness to cascade traversal logic instead of 
-- writing it out explicitly.
link ::  Ord a => PHeap a -> PHeap a -> PHeap a
link PHeap{oddh = Empty, ..} h2 = PHeap node h2 susp
link PHeap{..} h2               = PHeap node Empty $ pmerge (pmerge h2 oddh) susp

pmerge ::  Ord a => PHeap a -> PHeap a -> PHeap a
pmerge h Empty = h
pmerge Empty h = h
pmerge a@(PHeap node1 _ _) b@(PHeap node2 _ _)
    | node1 < node2 = link a b
    | otherwise     = link b a

instance Ord a => Heap (PHeap a) a where
    empty                 = Empty
    isEmpty Empty         = True
    isEmpty _             = False
    merge                 = pmerge
    insert e h            = merge (PHeap e Empty Empty) h
    findMin Empty         = Nothing
    findMin PHeap{ node } = Just node
    deleteMin Empty       = Nothing
    deleteMin PHeap{..}   = Just $ merge oddh susp
