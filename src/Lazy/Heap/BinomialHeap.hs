{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Heap.BinomialHeap(BHeap) where

import Heap.Heap

{-
    this implementation achieves insert of O(1) worst case complexity,
    merge, findMin, and deleteMin of O(lg n) worst case complexity.
-}

data Node a  = Node{node :: a , nlist :: [Node a]} deriving Show
data Digit a = Zero | One (Node a) deriving Show
data BHeap a = BHeap{digits :: [Digit a], schedule :: [[Digit a]]} deriving Show

pattern Empty sched = BHeap [] sched


link ::  Ord a => Node a -> Node a -> Node a
link nd1@(Node n1 nl1) nd2@(Node n2 nl2)
    | n1 <= n2  = Node n1 $ nd2:nl1
    | otherwise = Node n2 $ nd1:nl2


insTree ::  Ord a => Node a -> [Digit a] -> [Digit a]
insTree t []          = [One t]
insTree t (Zero:ds)   = One t : ds
insTree t (One t':ds) = Zero : insTree (link t t') ds


exec ::  [[Digit t]] -> [[Digit t]]
exec []                 = []
exec ((One _:_):sched)  = sched
exec ((Zero:job):sched) = job:sched


mrg ::  Ord a => [Digit a] -> [Digit a] -> [Digit a]
mrg ds [] = ds
mrg [] ds = ds
mrg (Zero:ds1) (d:ds2) = d:mrg ds1 ds2
mrg (d:ds1) (Zero:ds2) = d:mrg ds1 ds2
mrg (One n1:ds1) (One n2:ds2) = (Zero:) $ insTree (link n1 n2) $ mrg ds1 ds2


-- |this function use pattern to force all computation
normalize ::  [t] -> [t]
normalize []    = []
normalize (h:t) = (h:) $! normalize t


rmMinTree ::  Ord a => [Digit a] -> (Node a, [Digit a])
rmMinTree [One n]                       = (n, [])
rmMinTree (Zero:(rmMinTree -> (n, ds))) = (n, Zero:ds)
rmMinTree (One n@Node{..}:tl@(rmMinTree -> (n'@(Node nd' _), ds')))
    | node < nd' = (n, Zero:tl)
    | otherwise  = (n', One n:ds')


instance (Ord a) => Heap (BHeap a) a where
    empty              = Empty []
    isEmpty (Empty _)  = True
    isEmpty _          = False
    insert e BHeap{..} = BHeap ds' $ exec $ exec $ ds':schedule
        where ds' = insTree (Node e []) digits

    merge (BHeap ds1 _) (BHeap ds2 _) = BHeap ds' []
        where ds' = normalize $ mrg ds1 ds2

    findMin (Empty _) = Nothing
    findMin BHeap{..} = Just node
        where (Node{..}, _) = rmMinTree digits

    deleteMin (Empty _) = Nothing
    deleteMin BHeap{..} = Just $ BHeap ds' []
        where (Node{..}, ds) = rmMinTree digits
              ds'            = mrg (map One $ reverse nlist) ds
