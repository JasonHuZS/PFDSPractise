{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.RealTimeDeque(RTDeque) where

import Queue.Queue
import Queue.Deque
import Data.List

{- 
    the same as banker's deque, but its overall complexity is O(1) in worst case.
-}


-- |constant in variance
-- shall be 2 or 3
c :: Integral a => a
c = 2


data RTDeque a = RTDeque{ flen   :: Integer
                        , flist  :: [a]
                        , fsched :: [a]
                        , rlen   :: Integer
                        , rlist  :: [a]
                        , rsched :: [a] } deriving Show

-- |use pattern matching to force computation
exec1 ::  [t] -> [t]
exec1 [] = []
exec1 (_:t) = t

exec2 ::  [t] -> [t]
exec2 = exec1 . exec1


-- |incremental (++ reverse) op
rotateRev ::  [a] -> [a] -> [a] -> [a]
rotateRev [] r acc    = (reverse r) ++ acc
rotateRev (x:f) r acc = let (t', r') = genericSplitAt c r
                         in (x:) $ rotateRev f r' $ t' ++ acc


rotateDrop ::  Integral i => [a] -> i -> [a] -> [a]
rotateDrop f j r
    | j < c     = rotateRev f (genericDrop j r) []
    | otherwise = let (x:f') = f
                   in (x:) $ rotateDrop f' (j - c) $ genericDrop c r


invariance ::  RTDeque a -> RTDeque a
invariance q@RTDeque{..}
    | flen > c * rlen + 1 = 
        let len    = flen + rlen
            i      = len `div` 2
            j      = len - i
            (f, r) = (genericTake i flist, rotateDrop rlist i flist)
         in RTDeque i f f j r r
    | rlen > c * flen + 1 =
        let len    = flen + rlen
            j      = len `div` 2
            i      = len - j
            (f, r) = (rotateDrop flist j rlist, genericTake j rlist)
         in RTDeque i f f j r r
    | otherwise          = q


instance Queue (RTDeque a) a where
    empty = RTDeque 0 [] [] 0 [] []
    isEmpty RTDeque{..} = flen + rlen == 0
    snoc q@RTDeque{..} e = invariance q{ rlen   = rlen + 1
                                       , rlist  = e:rlist
                                       , fsched = exec1 fsched
                                       , rsched = exec1 rsched }
    head RTDeque{flist, rlist}
        | [] <- flist, [] <- rlist    = Nothing
        | [] <- flist, (h:_) <- rlist = Just h
        | (h:_) <- flist              = Just h
    tail q@RTDeque{..}
        | [] <- flist, [] <- rlist    = Nothing
        | [] <- flist, (_:_) <- rlist = Just empty
        | (_:t) <- flist              = Just $ invariance q{ flen  = flen + 1
                                                           , flist  = t
                                                           , fsched = exec2 fsched
                                                           , rsched = exec2 rsched }
    

instance Deque (RTDeque a) a where
    cons e q@RTDeque{..} = invariance q{ flen = flen + 1
                                       , flist = e:flist
                                       , fsched = exec1 fsched
                                       , rsched = exec1 rsched }
    last RTDeque{flist, rlist}
        | [] <- rlist, [] <- flist    = Nothing
        | [] <- rlist, (h:_) <- flist = Just h
        | (h:_) <- rlist              = Just h
    init q@RTDeque{..}
        | [] <- rlist, [] <- flist    = Nothing
        | [] <- rlist, (_:_) <- flist = Just empty
        | (_:t) <- rlist              = Just $ invariance q{ rlen  = rlen + 1
                                                           , rlist  = t
                                                           , fsched = exec2 fsched
                                                           , rsched = exec2 rsched }
