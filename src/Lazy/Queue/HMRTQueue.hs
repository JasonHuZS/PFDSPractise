{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.HMRTQueue(HMRTQueue) where

import Queue.Queue

{-
    Hood-Melville Real-Time Queue

using global rebuilding to achieve O(1) worst case overall complexity.

-}

data Rotation a = Idle 
                | Reversing{ validlen :: Integer
                           , frontl   :: [a]
                           , frontacc :: [a]
                           , rearl    :: [a]
                           , rearacc  :: [a] }
                | Appending{ validlen :: Integer
                           , frontacc :: [a]
                           , rearacc  :: [a] }
                | Done [a] deriving Show


data HMRTQueue a = HMRTQueue{ flen     :: Integer
                            , front    :: [a]
                            , rotation :: Rotation a
                            , rlen     :: Integer
                            , rear     :: [a] } deriving Show


exec ::  Rotation a -> Rotation a
exec Reversing{frontl = [], rearl = [y], ..} = Appending{rearacc = y:rearacc, ..}
exec Reversing{..} = let !vlen   = validlen + 1
                         (fh:ft) = frontl
                         (rh:rt) = rearl
                      in Reversing vlen ft (fh:frontacc) rt $ rh:rearacc
exec Appending{validlen = 0, ..} = Done rearacc
exec Appending{..} = let !vlen   = validlen - 1
                         (fh:ft) = frontacc
                      in Appending vlen ft $ fh:rearacc
exec s = s


invalidate ::  Rotation a -> Rotation a
invalidate Reversing{..}      = let !vlen = validlen - 1 in Reversing{validlen = vlen, ..}
invalidate Appending{validlen = 0, ..} = Done $ Prelude.tail rearacc
invalidate Appending{..}      = let !vlen = validlen - 1 in Appending{validlen = vlen, ..}
invalidate s = s


exec2 ::  HMRTQueue a -> HMRTQueue a
exec2 HMRTQueue{rotation = exec . exec -> state, ..}
    | Done nf <- state = HMRTQueue{front = nf, rotation = Idle, ..}
    | otherwise = HMRTQueue{rotation = state, ..}


check ::  HMRTQueue a -> HMRTQueue a
check q@HMRTQueue{..}
    | rlen <= flen = exec2 q
    | otherwise    = let !nlen = flen + rlen
                         rot   = Reversing 0 front [] rear []
                      in exec2 $ HMRTQueue nlen front rot 0 []


instance Queue (HMRTQueue a) a where
    empty                   = HMRTQueue 0 [] Idle 0 []
    isEmpty HMRTQueue{flen} = flen == 0
    snoc HMRTQueue{..} e    = check HMRTQueue{rlen = rlen + 1, rear = e:rear, ..}
    head HMRTQueue{front}
        | [] <- front    = Nothing
        | (h:_) <- front = Just h
    tail HMRTQueue{..}
        | flen == 0 = Nothing
        | otherwise = let !len = flen - 1
                       in Just $ check $ HMRTQueue{flen = len, rotation = invalidate rotation, ..}
