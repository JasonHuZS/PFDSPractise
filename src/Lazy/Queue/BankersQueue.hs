{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.BankersQueue(BQueue) where

import Queue.Queue


data BQueue a = BQueue { flen  :: Integer
                       , flist :: [a]
                       , rlen  :: Integer
                       , rlist :: [a] } deriving Show

pattern BEmpty = BQueue 0 [] 0 []


-- |this helper maintains the invariance of this queue, such that
-- it makes the overhead amortized to O(1).
-- 
-- generally speaking, the relation of rlen and flen can be expressed as 
--              rlen <= a * flen
-- where a is a constant factor, which influences the timing of reverse op.
--
-- the larger a is , the fewer times reverse is called, but the less stable 
--  the performance is.
invariance ::  BQueue t -> BQueue t
invariance q@BQueue{..}
    | rlen <= flen = q
    | otherwise    = BQueue (flen+rlen) (flist ++ reverse rlist) 0 []


-- |all amortized O(1) time complexity
instance Queue (BQueue a) a where
    empty                = BEmpty
    isEmpty BEmpty       = True
    isEmpty _            = False
    snoc q@BQueue{..} e  = invariance $ BQueue flen flist (rlen+1) $ e:rlist
    head BEmpty          = Nothing
    head BQueue{ flist } = Just $ Prelude.head flist
    tail BEmpty          = Nothing
    tail BQueue{..}      = Just $ invariance $ BQueue (flen-1) (Prelude.tail flist) rlen rlist

