{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.HMRTQueue2(HMRTQueue2) where

import Queue.Queue

{-
    Hood-Melville Real-Time Queue

using global rebuilding to achieve O(1) worst case overall complexity.

this is similar to the previous one, but each modifications only call `exec`
once, but still achieves the goal.
-}

data Rotation a = Idle 
                | Reversing{ validlen :: Integer
                           , frontl   :: [a]
                           , frontacc :: [a]
                           , rearl    :: [a]
                           , rearacc  :: [a] }
                | Appending{ validlen :: Integer
                           , frontacc :: [a]
                           , rearacc  :: [a] }
                | Done [a] deriving Show


data HMRTQueue2 a = HMRTQueue2{ flen     :: Integer
                              , front    :: [a]
                              , rotation :: Rotation a
                              , rlen     :: Integer
                              , rear     :: [a] } deriving Show


exec ::  Rotation a -> Rotation a
exec Reversing{validlen = 0, rearl = [y], ..} = Done $ y:rearacc
exec Reversing{frontl = [], rearl = [y], ..} = Appending{rearacc = y:rearacc, ..}
exec Reversing{..} = let !vlen   = validlen + 1
                         (fh:ft) = frontl
                         (rh:rt) = rearl
                      in Reversing vlen ft (fh:frontacc) rt $ rh:rearacc
exec Appending{..} = let !vlen   = validlen - 1
                         (fh:ft) = frontacc
                      in if vlen == 0 then Done $ fh:rearacc
                                      else Appending vlen ft $ fh:rearacc
exec s = s


invalidate ::  Rotation a -> Rotation a
invalidate Reversing{..}      = let !vlen = validlen - 1 in Reversing{validlen = vlen, ..}
invalidate Appending{validlen = 0, ..} = Done $ Prelude.tail rearacc
invalidate Appending{..}      = let !vlen = validlen - 1 in Appending{validlen = vlen, ..}
invalidate s = s


check ::  HMRTQueue2 a -> HMRTQueue2 a
check q@HMRTQueue2{..}
    | rlen <= flen = case exec rotation of
                       Done nf -> q {front = nf, rotation = Idle}
                       state   -> q {rotation = state}
    | otherwise    = let !nlen = flen + rlen
                         rot   = exec $ Reversing 0 front [] rear []
                      in case rot of 
                           Done nf -> HMRTQueue2 nlen nf Idle 0 []
                           state   -> HMRTQueue2 nlen front rot 0 []


instance Queue (HMRTQueue2 a) a where
    empty                    = HMRTQueue2 0 [] Idle 0 []
    isEmpty HMRTQueue2{flen} = flen == 0
    snoc HMRTQueue2{..} e    = check HMRTQueue2{rlen = rlen + 1, rear = e:rear, ..}
    head HMRTQueue2{front}
        | [] <- front    = Nothing
        | (h:_) <- front = Just h
    tail HMRTQueue2{..}
        | [] <- front        = Nothing
        | (_:front) <- front = 
            let !len = flen - 1
             in Just $ check $ HMRTQueue2{ flen     = len 
                                         , rotation = invalidate rotation
                                         , ..}
