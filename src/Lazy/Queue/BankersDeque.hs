{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.BankersDeque(BDeque, reverseDeque) where

import Queue.Queue
import Queue.Deque
import Data.List


data BDeque a = BDeque { flen  :: Integer
                       , flist :: [a]
                       , rlen  :: Integer
                       , rlist :: [a] } deriving Show

pattern BEmpty = BDeque 0 [] 0 []

-- |constant in invariance expression
c :: Num a => a
c = 2


-- |this function keep the invariance of this deque to be
--
--          flen <= c * rlen + 1
--      and rlen <= c * flen + 1
--
invariance ::  BDeque a -> BDeque a
invariance q@BDeque{..}
    | flen > c * rlen + 1 = 
        let len    = flen + rlen
            i      = len `div` 2
            j      = len - i
            (f, r) = (genericTake i flist, (rlist++) $ reverse $ genericDrop i flist)
         in BDeque i f j r
    | rlen > c * flen + 1 =
        let len    = flen + rlen
            j      = len `div` 2
            i      = len - j
            (f, r) = ((flist++) $ reverse $ genericDrop j rlist, genericTake j rlist)
         in BDeque i f j r
    | otherwise          = q


instance Queue (BDeque a) a where
    empty               = BEmpty
    isEmpty BEmpty      = True
    isEmpty _           = False
    snoc q@BDeque{..} e = invariance q{rlen = rlen + 1, rlist = e:rlist}
    head BEmpty         = Nothing
    head BDeque{flist, rlist} 
        | [] <- flist    = Just $ Prelude.head rlist
        | (h:_) <- flist = Just h
    tail BEmpty         = Nothing
    tail q@BDeque{..}
        | [] <- flist    = Just BEmpty
        | (_:t) <- flist = Just $ invariance q{flen = flen - 1, flist = t}


instance Deque (BDeque a) a where
    cons e q@BDeque{..} = invariance q{flen = flen + 1, flist = e:flist}
    last BEmpty         = Nothing
    last BDeque{flist, rlist}
        | [] <- rlist    = Just $ Prelude.head flist
        | (h:_) <- rlist = Just h
    init BEmpty         = Nothing
    init q@BDeque{..}
        | [] <- rlist    = Just BEmpty
        | (_:t) <- rlist = Just $ invariance q{rlen = rlen - 1, rlist = t}

-- |reverse the deque
reverseDeque ::  BDeque a -> BDeque a
reverseDeque BDeque{..} = BDeque rlen rlist flen flist
