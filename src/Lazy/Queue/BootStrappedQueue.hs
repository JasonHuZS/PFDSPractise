{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE RecordWildCards #-}

module Lazy.Queue.BootStrappedQueue(BSQueue) where

import Queue.Queue
import Prelude hiding (head, tail)

{-
this data structure bootstraps itself by inserting reverse to a suspension queue
-}


data BSQueue a = Empty
               | Queue{ flen  :: Integer
                      , flist :: [a]
                      , susp  :: BSQueue [a]
                      , rlen  :: Integer
                      , rlist :: [a] } deriving Show


checkf ::  BSQueue a -> BSQueue a
checkf q@Queue{flist = [], ..}
    | Empty <- susp  = Empty
    | Just h <- head susp
    , Just t <- tail susp = q{flist = h, susp = t}
checkf q = q


checkq ::  BSQueue a -> BSQueue a
checkq q@Queue{..}
    | rlen <= flen = checkf q
    | otherwise    = checkf q{ flen  = flen + rlen
                             , susp  = snoc susp $ reverse rlist
                             , rlen  = 0
                             , rlist = [] }


instance Queue (BSQueue a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    snoc Empty e  = Queue 1 [e] Empty 0 []
    snoc q e      = checkq $ q{rlen = 1 + rlen q, rlist = e:rlist q}

    head Empty    = Nothing
    head Queue{flist = (h:_)} = Just h

    tail Empty = Nothing
    tail q@Queue{flen, flist = (_:t)} = Just $ checkq $ q{flen = flen - 1, flist = t}
