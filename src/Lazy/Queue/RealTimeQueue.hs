{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternGuards #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.Queue.RealTimeQueue(RTQueue) where

import Queue.Queue

{-
    this data structure makes a BankersQueue implementation from O(1)
    amortized complexity yet O(n) worst case complexity to O(1) 
    worst case complexity.
-}

data RTQueue a = RTQueue{ front    :: [a]
                        , rear     :: [a]
                        , schedule :: [a] } deriving Show

pattern QEmpty = RTQueue [] [] []

-- |turn `++ reverse` operation into an incremental lazy operation.
-- this turns such queue from O(n) worst case complexity to O(lg n)
-- worst case complexity.
rotate ::  [a] -> [a] -> [a] -> [a]
rotate [] [y] a        = y:a
rotate (x:xs) (y:ys) a = x:(rotate xs ys $ y:a)

-- |turn the queue into a realtime one by restricting the dependency depth
-- to a constant, by forcing the computation explcitly.
exec ::  RTQueue a -> RTQueue a
exec RTQueue{..}
    | (_:xs) <- schedule = RTQueue{schedule = xs, ..}
    | otherwise          = f' `seq` RTQueue f' [] f'
        where f' = rotate front rear []
              -- f' is forced to be evaled to weak head NF here

instance Queue (RTQueue a) a where
    empty              = QEmpty
    isEmpty QEmpty     = True
    isEmpty _          = False
    snoc RTQueue{..} e = exec $ RTQueue front (e:rear) schedule
    head RTQueue{front}
        | (x:_) <- front = Just x
        | otherwise      = Nothing
    tail RTQueue{..}
        | (_:xs) <- front = Just $ exec $ RTQueue{front = xs, ..}
        | otherwise       = Nothing
