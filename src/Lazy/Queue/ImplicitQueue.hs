{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PatternSynonyms #-}

module Lazy.Queue.ImplicitQueue where

import Queue.Queue
import Prelude hiding (head, tail)
import Data.Maybe


data Digit a = Zero
             | One  { node1 :: a }
             | Two  { node1 :: a
                    , node2 :: a } deriving Show


data IQueue a = Shallow (Digit a)
              | Deep { hdigit :: Digit a
                     , susp   :: IQueue (a, a)
                     , tdigit :: Digit a } deriving Show


pattern Empty = Shallow Zero


instance Queue (IQueue a) a where
    empty                    = Empty
    isEmpty Empty            = True
    isEmpty _                = False
    snoc Empty e             = Shallow $ One e
    snoc (Shallow (One x)) e = Deep{hdigit = Two x e, susp = empty, tdigit = Zero}
    snoc q@Deep{tdigit = Zero} e  = q{tdigit = One e}
    snoc q@Deep{tdigit = One x} e = q{susp = snoc (susp q) (x, e), tdigit = Zero}
    head Empty             = Nothing
    head (Shallow (One x)) = Just x
    head Deep{hdigit}      = Just $ node1 hdigit
    tail Empty = Nothing
    tail (Shallow (One _)) = Just Empty
    tail q@Deep{..} = case hdigit of
        One{}
            | isEmpty susp -> Just $ Shallow tdigit
            | otherwise    -> Just q{ hdigit = Two x y
                                   , susp   = fromJust $ tail susp }
                                 where Just (x, y) = head susp
        Two{..} -> Just q{hdigit = One node2}
