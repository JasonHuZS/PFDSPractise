{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Lazy.RandomAccessList.ZerolessRedundantList2(ZRList2, toList2) where

import RandomAccessList.RandomAccessList
import Prelude hiding (lookup)

{- exercise 9.10
laziness aware, using zeroless representation with redundant binary number and scheduling,
cons, head, tail are in O(1) complexity in worst case.

cons and tail are symmetric in the sense of binary number representaiton, which correspond
to inc and dec, respectively.

since the amortized cost of cons, and tail are 2, therefore, each of them shall get scheduled
to execute two pendings each invocation to catch up.

thus, cons and tail can achive O(1) worst case complexity.
-}

data Tree a = Leaf a
            | Node{ _size :: Integer
                  , left  :: Tree a
                  , right :: Tree a } deriving Show

data Digit a = One  { tree1 :: Tree a }
             | Two  { tree1 :: Tree a
                    , tree2 :: Tree a }
             | Two' { tree1 :: Tree a  -- implies recursive call
                    , tree2 :: Tree a }
             | Three{ tree1 :: Tree a
                    , tree2 :: Tree a
                    , tree3 :: Tree a } deriving Show

data ZRList2 a = ZRList2{ list  :: [Digit a]
                      , sched :: [[Digit a]] } deriving Show

pattern Empty sched = ZRList2 [] sched


size ::  Tree t -> Integer
size (Leaf _)    = 1
size Node{_size} = _size


link ::  Tree a -> Tree a -> Tree a
link t1 t2 = Node (size t1 + size t2) t1 t2


exec ::  [[Digit t]] -> [[Digit t]]
exec []                     = []
exec ((Two'{..}:job):sched) = job:sched
exec (_:sched)              = sched


consTree ::  Tree a -> [Digit a] -> [Digit a]
consTree t []             = [One t]
consTree t (One hd:tl)    = Two t hd:tl
consTree t (Two{..}:tl)   = Three{tree1 = t, tree2 = tree1, tree3 = tree2}:tl
consTree t (Two'{..}:tl)  = Three{tree1 = t, tree2 = tree1, tree3 = tree2}:tl
consTree t (Three{..}:tl) = Two' t tree1:consTree (link tree2 tree3) tl


unconsTree ::  [Digit a] -> [Digit a]
unconsTree [One _]                   = []
unconsTree (Two _ t:ds)              = One t:ds
unconsTree (Two' _ t:ds)             = One t:ds
unconsTree (Three{..}:ds)            = Two tree2 tree3:ds
unconsTree (One _:One n@Node{..}:ds) = Two left right:unconsTree (One n:ds)
unconsTree (One _:d2:ds)             = let Node{..} = tree1 d2
                                        in Two' left right:(unconsTree $ d2:ds)


lookupTree ::  Integer -> Tree a -> Maybe a
lookupTree i (Leaf x)
    | i == 0    = Just x
    | otherwise = Nothing
lookupTree i Node{..}
    | i < delim = lookupTree i left
    | otherwise = lookupTree (i - delim) right
    where delim = _size `div` 2


updateTree ::  Integer -> a -> Tree a -> Maybe (Tree a)
updateTree i y (Leaf x)
    | i == 0    = Just $ Leaf y
    | otherwise = Nothing
updateTree i y n@Node{..}
    | i < delim = (\l -> n{left = l}) <$> updateTree i y left
    | otherwise = (\r -> n{right = r}) <$> updateTree i y right
    where delim = _size `div` 2


instance RandomAccessList (ZRList2 a) a where
    empty              = Empty []
    isEmpty (Empty _)  = True
    isEmpty _          = False
    cons e ZRList2{..} = ZRList2 ds $ exec $ exec $ ds:sched
        where ds       = consTree (Leaf e) list
    
    head (Empty _)    = Nothing
    head ZRList2{list = tree1 . Prelude.head -> Leaf x} = Just x

    tail (Empty _)   = Nothing
    tail ZRList2{..} = Just $ ZRList2 ds $ exec $ exec $ ds:sched
        where ds     = unconsTree list

    lookup i (Empty _) = Nothing
    lookup i l@ZRList2{list = One t:ds}
        | i < sz    = lookupTree i t
        | otherwise = lookup (i - sz) l{list = ds}
        where sz    = size t
    lookup i l@ZRList2{list = Two t1 t2:ds}
        | i < sz    = lookupTree i t1
        | i < sz2   = lookupTree (i - sz) t2
        | otherwise = lookup (i - sz2) l{list = ds}
        where sz    = size t1
              sz2   = 2 * sz
    lookup i l@ZRList2{list = Two' t1 t2:ds}
        | i < sz    = lookupTree i t1
        | i < sz2   = lookupTree (i - sz) t2
        | otherwise = lookup (i - sz2) l{list = ds}
        where sz    = size t1
              sz2   = 2 * sz
    lookup i l@ZRList2{list = Three{..}:ds}
        | i < sz    = lookupTree i tree1
        | i < sz2   = lookupTree (i - sz) tree2
        | i < sz3   = lookupTree (i - sz2) tree3
        | otherwise = lookup (i - sz3) l{list = ds}
        where sz    = size tree1
              sz2   = 2 * sz
              sz3   = sz + sz2

    update i y (Empty _) = Nothing
    update i y l@ZRList2{list = One t:ds}
        | i < sz    = (\n             -> l{list = One n:ds}) <$> updateTree i y t
        | otherwise = (\ZRList2{list} -> l{list = One t:list}) <$> (update (i - sz) y $ l{list = ds})
        where sz    = size t
    update i y l@ZRList2{list = Two t1 t2:ds}
        | i < sz    = (\n             -> l{list = Two n t2:ds}) <$> updateTree i y t1
        | i < sz2   = (\n             -> l{list = Two t1 n:ds}) <$> updateTree (i - sz) y t2
        | otherwise = (\ZRList2{list} -> l{list = Two t1 t2:list}) <$> (update (i - sz2) y $ l{list = ds})
        where sz    = size t1
              sz2   = 2 * sz
    update i y l@ZRList2{list = Two' t1 t2:ds}
        | i < sz    = (\n             -> l{list = Two' n t2:ds}) <$> updateTree i y t1
        | i < sz2   = (\n             -> l{list = Two' t1 n:ds}) <$> updateTree (i - sz) y t2
        | otherwise = (\ZRList2{list} -> l{list = Two' t1 t2:list}) <$> (update (i - sz2) y $ l{list = ds})
        where sz    = size t1
              sz2   = 2 * sz
    update i y l@ZRList2{list = d@Three{..}:ds}
        | i < sz    = (\n             -> l{list = d{tree1 = n}:ds}) <$> updateTree i y tree1
        | i < sz2   = (\n             -> l{list = d{tree2 = n}:ds}) <$> updateTree (i - sz) y tree2
        | i < sz3   = (\n             -> l{list = d{tree3 = n}:ds}) <$> updateTree (i - sz3) y tree3
        | otherwise = (\ZRList2{list} -> l{list = d:list}) <$> (update (i - sz3) y $ l{list = ds})
        where sz    = size tree1
              sz2   = 2 * sz
              sz3   = sz + sz2


toList2 ::  ZRList2 a -> [a]
toList2 ZRList2{list}                 = _toList list []
    where traverse (Leaf x) acc      = x:acc
          traverse Node{..} acc      = traverse left $ traverse right acc
          _toList [] acc             = acc
          _toList (One n:ds) acc     = traverse n $ _toList ds acc
          _toList (Two{..}:ds) acc   = traverse tree1 $ traverse tree2 $ _toList ds acc
          _toList (Two'{..}:ds) acc  = traverse tree1 $ traverse tree2 $ _toList ds acc
          _toList (Three{..}:ds) acc = traverse tree1 $ traverse tree2 $ traverse tree3 $ _toList ds acc
