## Laziness Aware Implementaions

these implementations take laziness of Haskell into account.

those ones not in this folder consider overhead in a strict evaluation manner,
which is not accurate at all.

the modules in this folder consider overhead in a lazy manner explicitly, 
such that the claimed amortized complexity is more precise.
