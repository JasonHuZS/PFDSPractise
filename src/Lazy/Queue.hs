module Lazy.Queue where

import Queue.Queue
import Queue.Deque
import Lazy.Queue.BankersQueue
import Lazy.Queue.RealTimeQueue
import Lazy.Queue.HMRTQueue
import Lazy.Queue.HMRTQueue2
import Lazy.Queue.BootStrappedQueue
import Lazy.Queue.ImplicitQueue
