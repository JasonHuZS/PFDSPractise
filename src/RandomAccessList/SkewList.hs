{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module RandomAccessList.SkewList where

import RandomAccessList.RandomAccessList
import Prelude hiding (lookup)

{-
  Skew numerical representation
head, tail, cons are O(1) in worst case
-}

data Tree a = Leaf a
            | Node{ left  :: Tree a
                  , node  :: a
                  , right :: Tree a } deriving Show

newtype SList a = SList [(Integer, Tree a)] deriving Show

pattern Empty = SList []


lookupTree ::  Integral a1 => a1 -> a1 -> Tree a -> Maybe a
lookupTree 0 _ (Leaf x)   = Just x
lookupTree i _ (Leaf _)   = Nothing
lookupTree 0 _ Node{node} = Just node
lookupTree i w Node{..}
    | i <= w'   = lookupTree (i - 1) w' left
    | otherwise = lookupTree (i - 1 - w') w' right
    where w'    = w `div` 2


updateTree :: Integral a1 => a1 -> a -> a1 -> Tree a -> Maybe (Tree a)
updateTree 0 y _ (Leaf x) = Just $ Leaf y
updateTree i _ _ (Leaf _) = Nothing
updateTree 0 y _ n        = Just n{node = y}
updateTree i y w n@Node{..}
    | i <= w'   = (\l -> n{left = l}) <$> updateTree (i - 1) y w' left
    | otherwise = (\r -> n{right = r}) <$> updateTree (i - 1 - w') y w' right
    where w'    = w `div` 2


instance RandomAccessList (SList a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    cons e (SList ((w1, t1):(w2, t2):ts))
        | w1 == w2    = SList $ (w1 + w2 + 1, Node t1 e t2):ts
    cons e (SList ts) = SList $ (1, Leaf e):ts

    head Empty                       = Nothing
    head (SList ((_, Leaf x):_))     = Just x
    head (SList ((_, Node{node}):_)) = Just node

    tail Empty                      = Nothing
    tail (SList ((_, Leaf _):ts))   = Just $ SList ts
    tail (SList ((w, Node{..}):ts)) = Just $ SList $ (w', left):(w', right):ts
        where w' = w `div` 2

    lookup _ Empty  = Nothing
    lookup i (SList ((w, t):ts))
        | i < w     = lookupTree i w t
        | otherwise = lookup (i - w) $ SList ts

    update _ _ Empty = Nothing
    update i y (SList (h@(w, t):ts))
        | i < w     = (\t'        -> SList $ (w, t'):ts) <$> updateTree i y w t
        | otherwise = (\(SList l) -> SList $ h:l) <$> update (i - w) y (SList ts)
