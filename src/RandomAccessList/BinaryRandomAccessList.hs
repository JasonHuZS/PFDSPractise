{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module RandomAccessList.BinaryRandomAccessList(BRList, toList2, drop, create) where

import RandomAccessList.RandomAccessList
import Data.Bits
import Prelude hiding (lookup, drop)

{-
binary random access list implemented as complete binary leaf tree
-}

data Tree a = Leaf a
            | Node{ _size :: Integer
                  , left  :: Tree a
                  , right :: Tree a } deriving Show

data Digit a = Zero | One (Tree a) deriving Show

newtype BRList a = BRList [Digit a] deriving Show

pattern Empty = BRList []

size ::  Tree t -> Integer
size (Leaf _)    = 1
size Node{_size} = _size

link ::  Tree a -> Tree a -> Tree a
link t1 t2 = Node (size t1 + size t2) t1 t2


consTree ::  Tree a -> [Digit a] -> [Digit a]
consTree t [] = [One t]
consTree t (Zero:tl) = One t:tl
consTree t (One hd:tl) = Zero:consTree (link t hd) tl


unconsTree ::  [Digit t] -> (Tree t, [Digit t])
unconsTree [One t]    = (t, [])
unconsTree (One t:tl) = (t, tl)
unconsTree (Zero:tl)  = let (Node{left, right}, tl') = unconsTree tl
                        in (left, One right:tl')


lookupTree ::  Integer -> Tree a -> Maybe a
lookupTree i (Leaf x)
    | i == 0    = Just x
    | otherwise = Nothing
lookupTree i Node{..}
    | i < delim = lookupTree i left
    | otherwise = lookupTree (i - delim) right
    where delim = _size `div` 2


updateTree ::  Integer -> a -> Tree a -> Maybe (Tree a)
updateTree i y (Leaf x)
    | i == 0    = Just $ Leaf y
    | otherwise = Nothing
updateTree i y n@Node{..}
    | i < delim = (\l -> n{left = l}) <$> updateTree i y left
    | otherwise = (\r -> n{right = r}) <$> updateTree (i - delim) y right
    where delim = _size `div` 2


instance RandomAccessList (BRList a) a where
    empty             = BRList []
    isEmpty Empty     = True
    isEmpty _         = False
    cons e (BRList l) = BRList $ consTree (Leaf e) l
    head Empty        = Nothing
    head (BRList l)   = let (Leaf x, _) = unconsTree l in Just x
    tail Empty        = Nothing
    tail (BRList l)   = let (_, tl) = unconsTree l in Just $ BRList tl

    lookup i Empty              = Nothing
    lookup i (BRList (Zero:tl)) = lookup i $ BRList tl
    lookup i (BRList (One t:tl))
        | i < sz    = lookupTree i t
        | otherwise = lookup (i - sz) $ BRList tl
        where sz    = size t

    update i y Empty = Nothing
    update i y (BRList (Zero:tl)) = update i y $ BRList tl
    update i y (BRList (One t:tl))
        | i < sz    = (\n -> BRList (One n:tl)) <$> updateTree i y t
        | otherwise = (\(BRList l) -> BRList (One t:l)) <$> (update (i - sz) y $ BRList tl)
        where sz    = size t


toList2 ::  BRList a -> [a]
toList2 (BRList l) = _toList l []
    where insertNode (Leaf x) acc = x:acc
          insertNode Node{..} acc = insertNode left $ insertNode right acc
          _toList [] acc          = acc
          _toList (Zero:tl) acc   = _toList tl acc
          _toList (One n:tl) acc  = insertNode n $ _toList tl acc


data Path = L | R | N deriving Show

-- |exercise 9.1 drop first n elems from the list
drop ::  Integer -> BRList t -> BRList t
drop n Empty          = Empty
drop n ral@(BRList l) = BRList $ _drop n l
    where mayPad0 []  = []
          mayPad0 l   = Zero:l

          _drop 0 l        = l
          _drop n []       = []
          _drop n (Zero:t) = _drop n t
          _drop n (One h:t)
            | size h < n = _drop (n - size h) t
            | otherwise  = dropTree n h t N

          dropTree 1 (Leaf _) acc p = case p of
                                        R -> Zero:acc
                                        L -> acc
                                        N -> mayPad0 acc
          dropTree n Node{..} acc p
            | n <= delim = case p of
                             L -> dropTree n left (One right:acc) L
                             _ -> dropTree n left (One right:mayPad0 acc) L
            | otherwise  = dropTree (n - delim) right (mayPad0 acc) R
            where delim  =  _size `div` 2


-- |exercise 9.2 create n copies of e
create ::  Integer -> t -> BRList t
create 0 _ = Empty
create n e = BRList $ _create 1 $ Leaf e
    where _create acc t
            | acc > n        = []
            | acc .&. n == 0 = (Zero:) $ _create acc2 $ Node acc2 t t
            | otherwise      = (One t:) $ _create acc2 $ Node acc2 t t
            where acc2 = acc * 2
