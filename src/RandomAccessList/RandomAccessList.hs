{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module RandomAccessList.RandomAccessList where

import Prelude hiding (head, tail)
import Control.Monad

class RandomAccessList l a | l -> a where
    empty   :: l
    isEmpty :: l -> Bool
    cons    :: a -> l -> l
    head    :: l -> Maybe a
    tail    :: l -> Maybe l
    lookup  :: Integer -> l -> Maybe a
    update  :: Integer -> a -> l -> Maybe l


fromList :: (RandomAccessList l a) => [a] -> l
fromList = foldr cons empty


toList' ::  RandomAccessList l a => l -> Maybe [a]
toList' l
    | isEmpty l = Just []
    | otherwise = liftM2 (:) (head l) $ tail l >>= toList'


toList ::  RandomAccessList a a1 => a -> [a1]
toList (toList' -> Just l) = l
toList _                   = []
