{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module RandomAccessList.NonuniformList where

import RandomAccessList.RandomAccessList
import Prelude hiding (lookup)

{-
  this implementation use non-uniform definition to enforce balance leaf tree property.
  it's a form of strutural decomposition.
  all operations run in O(lg n) worst case complexity
-}

data Seq a = Nil 
           | Zero{ sub  :: Seq (a, a) }
           | One { node :: a
                 , sub  :: Seq (a, a) } deriving Show


uncons :: Seq a -> (a, Seq a)
uncons One{node, sub = Nil} = (node, Nil)
uncons One{..}              = (node, Zero sub)
uncons Zero{sub}            = (x, One y sub')
    where ((x, y), sub') = uncons sub


-- |this one needs to call lookup, so it's kind of slow
origUpdate ::  Integer -> t -> Seq t -> Maybe (Seq t)
origUpdate _ _ Nil       = Nothing
origUpdate 0 y s@One{}   = Just s{node = y}
origUpdate i y s@One{..} = cons node <$> update (i - 1) y (Zero sub)
origUpdate i y s@Zero{sub}
    | Just (a, b) <- lookup (i `div` 2) sub = 
        let p = if i `mod` 2 == 0 then (y, b) else (a, y)
            in Zero <$> update (i `div` 2) p sub
    | otherwise = Nothing


fupdate :: (a -> a) -> Integer -> Seq a -> Maybe (Seq a)
fupdate _ _ Nil         = Nothing
fupdate f 0 s@One{node} = Just s{node = f node}
fupdate f i s@One{..}   = cons node <$> fupdate f (i - 1) (Zero sub)
fupdate f i s@Zero{sub} = let f' (x, y) = if i `mod` 2 == 0 then (f x, y) else (x, f y)
                           in Zero <$> fupdate f' (i `div` 2) sub


instance RandomAccessList (Seq a) a where
    empty            = Nil
    isEmpty Nil      = True
    isEmpty _        = False
    cons x Nil       = One x Nil
    cons x Zero{sub} = One x sub
    cons x One{..}   = Zero $ cons (x, node) sub
    head Nil         = Nothing
    head seq         = Just $ fst $ uncons seq
    tail Nil         = Nothing
    tail seq         = Just $ snd $ uncons seq

    lookup _ Nil       = Nothing
    lookup 0 One{node} = Just node
    lookup i One{sub}  = lookup (i - 1) $ Zero sub
    lookup i Zero{sub}
        | Just (x, y) <- lookup (i `div` 2) sub =
            if | i `mod` 2 == 0 -> Just x
               | otherwise      -> Just y
        | otherwise = Nothing
    
    update i y seq = fupdate (\x -> y) i seq
