{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module RandomAccessList.ZerolessRandomAccessList(ZRList) where

import RandomAccessList.RandomAccessList
import Prelude hiding (lookup)

{- exercise 9.5
  use zeroless representation to implment O(1) head,
  while achieve slightly better performance of lookup and update too.
-}

data Tree a = Leaf a
            | Node{ _size :: Integer
                  , left  :: Tree a
                  , right :: Tree a } deriving Show

data Digit a = One (Tree a) | Two (Tree a) (Tree a) deriving Show

newtype ZRList a = ZRList [Digit a] deriving Show

pattern Empty = ZRList []


size ::  Tree t -> Integer
size (Leaf _)    = 1
size Node{_size} = _size


link ::  Tree a -> Tree a -> Tree a
link t1 t2 = Node (size t1 + size t2) t1 t2


consTree ::  Tree a -> [Digit a] -> [Digit a]
consTree t [] = [One t]
consTree t (One hd:tl) = Two t hd:tl
consTree t (Two h1 h2:tl) = One t:consTree (Node (size h1 + size h2) h1 h2) tl


unconsTree ::  [Digit a] -> [Digit a]
unconsTree [One _] = []
unconsTree (Two _ t:ds) = One t:ds
unconsTree (One _:One n@Node{..}:ds) = Two left right:unconsTree (One n:ds)
unconsTree (One _:Two Node{..} t2:ds) = Two left right:One t2:ds


lookupTree ::  Integer -> Tree a -> Maybe a
lookupTree i (Leaf x)
    | i == 0    = Just x
    | otherwise = Nothing
lookupTree i Node{..}
    | i < delim = lookupTree i left
    | otherwise = lookupTree (i - delim) right
    where delim = _size `div` 2


updateTree ::  Integer -> a -> Tree a -> Maybe (Tree a)
updateTree i y (Leaf x)
    | i == 0    = Just $ Leaf y
    | otherwise = Nothing
updateTree i y n@Node{..}
    | i < delim = (\l -> n{left = l}) <$> updateTree i y left
    | otherwise = (\r -> n{right = r}) <$> updateTree i y right
    where delim = _size `div` 2


instance RandomAccessList (ZRList a) a where
    empty                            = Empty
    isEmpty Empty                    = True
    isEmpty _                        = False
    cons e (ZRList l)                = ZRList $ consTree (Leaf e) l
    head Empty                       = Nothing
    head (ZRList (One (Leaf x):_))   = Just x
    head (ZRList (Two (Leaf x) _:_)) = Just x
    tail Empty                       = Nothing
    tail (ZRList l)                  = Just $ ZRList $ unconsTree l

    lookup i Empty  = Nothing
    lookup i (ZRList (One t:ds))
        | i < sz    = lookupTree i t
        | otherwise = lookup (i - sz) $ ZRList ds
        where sz    = size t
    lookup i (ZRList (Two t1 t2:ds))
        | i < sz    = lookupTree i t1
        | i < sz2   = lookupTree (i - sz) t2
        | otherwise = lookup (i - sz2) $ ZRList ds
        where sz    = size t1
              sz2   = 2 * sz

    update i y Empty = Nothing
    update i y (ZRList (One t:ds))
        | i < sz    = (\n          -> ZRList $ One n:ds) <$> updateTree i y t
        | otherwise = (\(ZRList l) -> ZRList $ One t:l) <$> (update (i - sz) y $ ZRList ds)
        where sz    = size t
    update i y (ZRList (Two t1 t2:ds))
        | i < sz    = (\n          -> ZRList $ Two n t2:ds) <$> updateTree i y t1
        | i < sz2   = (\n          -> ZRList $ Two t1 n:ds) <$> updateTree (i - sz) y t2
        | otherwise = (\(ZRList l) -> ZRList $ Two t1 t2:l) <$> (update (i - sz2) y $ ZRList ds)
        where sz    = size t1
              sz2   = 2 * sz
