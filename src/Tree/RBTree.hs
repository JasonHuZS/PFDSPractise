{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}

module Tree.RBTree(Color(..), RBTree, fromOrdList) where

import Data.List hiding (insert)
import Tree.Set

data Color = R | B deriving Show
data RBTree a = Empty | RBTree { color :: Color
                               , ltree :: RBTree a
                               , nod   :: a
                               , rtree :: RBTree a } deriving Show


rbColor ::  RBTree t -> Color
rbColor Empty               = B
rbColor RBTree{ color = c } = c

-- |balance one layer of subtree.
-- fix violation of "all parents of reds are black"
-- majorly deal with black-red-red path
rbBalance ::  RBTree a -> RBTree a
rbBalance Empty = Empty
{-
            B-z
            / \
          R-y  d
          / \
        R-x  c
        / \
       a   b
-}
rbBalance (RBTree B (RBTree R (RBTree R a x b) y c) z d) = RBTree R (RBTree B a x b) y (RBTree B c z d)
{-
            B-z
            / \
          R-x  d
          / \
         a  R-y
            / \
           b   c
-}
rbBalance (RBTree B (RBTree R a x (RBTree R b y c)) z d) = RBTree R (RBTree B a x b) y (RBTree B c z d)
{-
            B-x
            / \
           a  R-z
              / \
            R-y  d
            / \
           b   c
-}
rbBalance (RBTree B a x (RBTree R (RBTree R b y c) z d)) = RBTree R (RBTree B a x b) y (RBTree B c z d)
{-
            B-x
            / \
           a  R-y
              / \
             b  R-z
                / \
               c   d
-}
rbBalance (RBTree B a x (RBTree R b y (RBTree R c z d))) = RBTree R (RBTree B a x b) y (RBTree B c z d)
{- all four above shall be converted to:
            R-y
           /   \
         B-x   B-z
         / \   / \
        a   b c   d
-}
rbBalance t = t

rbInsert ::  Ord a => a -> RBTree a -> RBTree a
rbInsert e s =
    let (RBTree _ l n r) = ins s
    in RBTree B l n r
    where ins Empty = RBTree R Empty e Empty
          ins s'@(RBTree c l n r)
            | e < n     = rbBalance $ RBTree c (ins l) n r
            | e > n     = rbBalance $ RBTree c l n $ ins r
            | otherwise = s'

instance (Ord a) => Set (RBTree a) a where
    empty      = Empty
    member e s = _member s (\_ -> False)
        where _member Empty pred' = pred' ()
              _member (RBTree _ l n r) pred'
                | e <= n    = _member l (\_ -> e == n)
                | otherwise = _member r pred'
    insert = optRBInsert2

-- 3.9 convert a sorted list(ascending order) to red black tree in O(n) time
-- |the idea here is divide and conquer.
-- during split, keep track of the mid point of the list
-- color maintaining is quite trivial: just color everything black, except some leaves red
fromOrdList ::  Ord e => [e] -> RBTree e
fromOrdList [] = empty
fromOrdList lst = 
    let (res, _) = _fol lst $ length lst
    in res
    where _fol :: (Ord e, Integral a) => [e] -> a -> (RBTree e, Maybe (e, [e]))
          _fol l 0            = (empty, uncons l)
          _fol (h:l) 1        = (RBTree B empty h empty, uncons l)
          _fol (h1:h2:l) 2    = (RBTree B (RBTree R empty h1 empty) h2 empty, uncons l)
          _fol (h1:h2:h3:l) 3 = (RBTree B (RBTree R empty h1 empty) h2 (RBTree R empty h3 empty), uncons l)
          _fol l n            =
            let mid                  = n `div` 2
                (ltr, Just (rt, tl)) = _fol l mid
                (rtr, mayremain)     = _fol tl (n - 1 - mid)
            in (RBTree B ltr rt rtr, mayremain)

-- 3.10 optimize insertion
-- (a)
-- |optimize it by recognizing balance subtree
optRBInsert ::  Ord a => a -> RBTree a -> RBTree a
optRBInsert e s = 
    let (RBTree _ l n r) = ins s
    in RBTree B l n r
    where lbalance Empty = Empty
          lbalance (RBTree B (RBTree R (RBTree R a x b) y c) z d) = 
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          lbalance (RBTree B (RBTree R a x (RBTree R b y c)) z d) = 
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          lbalance t = t

          rbalance Empty = Empty
          rbalance (RBTree B a x (RBTree R (RBTree R b y c) z d)) =
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          rbalance (RBTree B a x (RBTree R b y (RBTree R c z d))) =
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          rbalance t = t

          ins Empty = RBTree R Empty e Empty
          ins s'@(RBTree c l n r)
            | e < n     = lbalance $ RBTree c (ins l) n r
            | e > n     = rbalance $ RBTree c l n $ ins r
            | otherwise = s'


-- (b)
data Direction = L | NL -- left or not left

-- | optimize further by totally eliminating guessing by offering explicit path track
optRBInsert2 ::  Ord a => a -> RBTree a -> RBTree a
optRBInsert2 e s = 
    let (_, RBTree _ l n r) = ins s
    in RBTree B l n r
    where llbalance (RBTree B (RBTree R (RBTree R a x b) y c) z d) = 
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          llbalance t = t
          lrbalance (RBTree B (RBTree R a x (RBTree R b y c)) z d) = 
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          lrbalance t = t

          rlbalance (RBTree B a x (RBTree R (RBTree R b y c) z d)) =
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          rlbalance t = t
          rrbalance (RBTree B a x (RBTree R b y (RBTree R c z d))) =
            RBTree R (RBTree B a x b) y (RBTree B c z d)
          rrbalance t = t

          ins Empty = (NL, RBTree R Empty e Empty)
          ins s'@(RBTree c l n r)
            | e < n     = 
                case ins l of
                    (L, t) -> (L, llbalance $ RBTree c t n r)
                    (_, t) -> (L, lrbalance $ RBTree c t n r)
            | e > n     =
                case ins r of
                    (L, t) -> (NL, rlbalance $ RBTree c l n t)
                    (_, t) -> (NL, rrbalance $ RBTree c l n t)
            | otherwise = (NL, s')
