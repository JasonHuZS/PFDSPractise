{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Tree.UnbalancedSet(UnbalancedSet, complete, complete2) where

import Tree.Set
import Data.List hiding (insert)

data Tree a = Empty | Tree { left :: Tree a, node :: a, right :: Tree a } deriving Show

type UnbalancedSet = Tree

-- worst case 2d comparisons, d is the depth of the tree
origMember :: (Ord a) => a -> (UnbalancedSet a) -> Bool
origMember _ Empty = False
origMember e (Tree l n r)
    | e == n = True
    | e < n  = member e l
    | e > n  = member e r

-- Exercise 2.2
-- worst case d+1 comparisons
-- keep a candidate to reduce comparison by half
optMember :: (Ord a) => a -> (UnbalancedSet a) -> Bool
optMember e set = _optMember set (\_ -> False)
    where _optMember Empty pred' = pred' ()
          _optMember (Tree l n r) pred'
            | e <= n = _optMember l (\_ -> e == n)
            | e > n  = _optMember r pred'

-- worst case 2d comparisons insertion which unnecessary copying
origInsert :: (Ord a) => a -> (UnbalancedSet a) -> (UnbalancedSet a)
origInsert e Empty = Tree Empty e Empty
origInsert e set@(Tree l n r)
    | e == n = set
    | e < n  = Tree (insert e l) n r
    | e > n  = Tree l n $ insert e r

-- 2.3 skipped. Haskell doesn't have the same exception construct as SML
-- 2.4 worst case d+1 comparison
optInsert :: (Ord a) => a -> (UnbalancedSet a) -> (UnbalancedSet a)
optInsert e set = _optInsert set (\_ -> True)
    where _optInsert Empty pred'
            | pred' ()  = Tree Empty e Empty
            | otherwise = Empty
          _optInsert (Tree l n r) pred'
            | e <= n = Tree (_optInsert l (\_ -> e /= n)) n r
            | e > n  = Tree l n $ _optInsert r pred'

instance (Ord a) => Set (UnbalancedSet a) a where
    empty = Empty
    insert = optInsert
    member = optMember

-- 2.5 (a)
complete :: (Ord a, Integral b) => a -> b -> Tree a
complete _ 0     = Empty
complete e depth = Tree subtree e subtree where subtree = complete e (depth - 1)

-- 2.5 (b)
complete2 :: (Ord a, Integral b) => a -> b -> Tree a
complete2 e size = case create2 size of (ret, _) -> ret
    where create2 0     = (Empty, Tree Empty e Empty)
          create2 depth = 
            let remain  = depth - 1
                (smaller, bigger) = create2 $ remain `div` 2
            in if even remain 
               then (Tree smaller e smaller, Tree smaller e bigger)
               else (Tree smaller e bigger, Tree bigger e bigger)


