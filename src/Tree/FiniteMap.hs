{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Tree.FiniteMap(FiniteMap(..), KVPair, Mapping) where

data Tree a = Empty | Tree { left :: Tree a, node :: a, right :: Tree a } deriving Show

-- 2.6 finite maps
class FiniteMap m k v | m -> k v where
    blank  :: m
    bind   :: k -> v -> m -> m
    lookup :: k -> m -> Maybe v
    
type KVPair k v = (k, v)
type Mapping k v = Tree (KVPair k v)

instance (Ord k) => FiniteMap (Mapping k v) k v where
    blank = Empty

    bind k v Empty = Tree Empty (k, v) Empty
    bind k v (Tree l n'@(k', _) r)
        | k == k'   = Tree l (k, v) r
        | k <= k'   = Tree (bind k v l) n' r
        | otherwise = Tree l n' $ bind k v r
{-
    bind k v mapping = _bind mapping (\_ -> True)
        where _bind Empty pred'
                | pred' ()  = Tree Empty (k, v) Empty
                | otherwise = Empty
              _bind (Tree l n'@(k', _) r) pred'
                | k <= k'   = Tree (_bind l (\_ -> if k == k' then )) n' r
                | otherwise = Tree l n' $ _bind r pred'
-}
    lookup k mapping = _lookup mapping (\_ -> Nothing)
        where _lookup Empty res = res ()
              _lookup (Tree l (k', v') r) res
                | k <= k'   = _lookup l (\_ -> if k == k' then Just v' else Nothing)
                | otherwise = _lookup r res


