{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Tree.Trie(Trie) where

import Tree.FiniteMap (FiniteMap(..))
import Prelude hiding (lookup)

data Trie m k v = Trie{ value :: Maybe v
                      , nodes :: m k (Trie m k v) }


instance (Show k, Show v, Show (m k (Trie m k v))) => Show (Trie m k v) where
    show Trie{..} = "Trie{value = " ++ show value ++ ", nodes = " ++ show nodes ++ "}"


instance (FiniteMap (m k (Trie m k v)) k (Trie m k v)) => FiniteMap (Trie m k v) [k] v where
    blank                  = Trie Nothing blank
    bind [] v t            = t{value = Just v}
    bind (k:ks) v Trie{..} =
        let t  = case lookup k nodes of 
                     Nothing -> blank
                     Just t' -> t'
            tt = bind k (bind ks v t) nodes
         in Trie value tt

    lookup [] Trie{value} = value
    lookup (k:ks) Trie{nodes} = lookup k nodes >>= lookup ks
