{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Tree.Set where

class Set s e | s -> e where
    empty  :: s
    insert :: e -> s -> s
    member :: e -> s -> Bool
