{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Utils.InfoHolder where


class Ord p => InfoHolder h p v | h -> p v where
    priority :: h -> p
    value    :: h -> v


instance (Ord p, InfoHolder h p v) => Eq h where
    h1 == h2 = priority h1 == priority h2


instance (Ord p, InfoHolder h p v) => Ord h where
    compare h1 h2 = compare (priority h1) $ priority h2
