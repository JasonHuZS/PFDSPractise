{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fno-warn-unused-binds #-}

module Stack(List(..), Stack, update, suffixes) where


-- main abstraction for lists
class Stack s e | s -> e where
    (++)    :: s -> s -> s
    empty   :: s
    isEmpty :: s -> Bool
    cons    :: e -> s -> s
    head    :: s -> Maybe e
    tail    :: s -> Maybe s


data List a = Nil | Cons a (List a) deriving Show

instance Stack (List e) e where
    empty = Nil

    isEmpty Nil = True
    isEmpty _   = False
    
    cons = Cons
    
    head Nil        = Nothing
    head (Cons e _) = Just e

    tail Nil         = Nothing
    tail (Cons _ es) = Just es

    Nil ++ l2        = l2
    (Cons h t) ++ l2 = Cons h $ t Stack.++ l2


instance Eq a => Eq (List a) where
    Nil          == Nil          = True
    _            == Nil          = False
    Nil          == _            = False
    (Cons h1 t1) == (Cons h2 t2) = h1 == h2 && t1 == t2


update :: (Integral b) => List a -> b -> a -> List a
update Nil _ _         = Nil
update (Cons _ t) 0 e  = Cons e t
update (Cons h t) i e  = Cons h $ update t (i - 1) e


-- Exercises
-- 2.1
suffixes :: List a -> List (List a)
suffixes Nil = Nil
suffixes l@(Cons _ t) = Cons l $ suffixes t
