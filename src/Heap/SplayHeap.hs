{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Heap.SplayHeap(SpTree, spSort) where

import Heap.Heap


data SpTree a = Empty | SpTree (SpTree a) a (SpTree a) deriving Show

spbigger ::  Ord a => a -> SpTree a -> SpTree a
spbigger _ Empty = Empty
spbigger e t@(SpTree l x r)
    | x <= e    = spbigger e r
    | otherwise = case l of
    Empty -> t
    SpTree a y b
        | y <= e    -> SpTree (spbigger e b) x r
        | otherwise -> SpTree (spbigger e a) y $ SpTree b x r

-- 5.4
spsmaller ::  Ord a => a -> SpTree a -> SpTree a
spsmaller _ Empty = Empty
spsmaller e t@(SpTree l x r)
    | x > e = spsmaller e l
    | otherwise = case r of 
    Empty -> t
    SpTree a y b
        | y > e     -> SpTree l x $ spsmaller e a
        | otherwise -> SpTree (SpTree l x a) y $ spsmaller e b


-- |since spsmaller and spbigger traverse the same path, so 
-- both step can be finished within one traversal.
sppartition ::  Ord a => a -> SpTree a -> (SpTree a, SpTree a)
sppartition _ Empty = (Empty, Empty)
sppartition e t@(SpTree l x r)
    | x <= e    = case r of
        Empty -> (t, Empty)
        SpTree a y b
            | y < e     -> let (sml, big) = sppartition e b
                           in (SpTree (SpTree l x a) y sml, big)
            | otherwise -> let (sml, big) = sppartition e a
                           in (SpTree l x sml, SpTree big y b)
    | otherwise = case l of
        Empty -> (Empty, t)
        SpTree a y b
            | y < e     -> let (sml, big) = sppartition e b
                           in (SpTree a y sml, SpTree big x r)
            | otherwise -> let (sml, big) = sppartition e a
                           in (sml, SpTree big y $ SpTree b x r)


instance Ord a => Heap (SpTree a) a where
    empty                  = Empty
    isEmpty Empty          = True
    isEmpty _              = False
    insert e h             = let (l, r) = sppartition e h in SpTree l e r
    merge Empty t          = t
    merge t Empty          = t
    merge (SpTree a x b) t = let (c, d) = sppartition x t
                             in SpTree (merge a c) x $ merge b d
    -- |in current implementation, the search path is as long as left spane, worst case O(n).
    -- however, change it to explicit min, the complexity will decrease to O(1).
    findMin Empty              = Nothing
    findMin (SpTree Empty m _) = Just m
    findMin (SpTree l _ _)     = findMin l

    deleteMin Empty                                           = Nothing
    deleteMin (SpTree Empty _ t)                              = Just t
    deleteMin (SpTree (SpTree Empty _ t) x r)                 = Just $ SpTree t x r
    deleteMin (SpTree (SpTree (deleteMin -> Just a) x b) y c) = Just $ SpTree a x $ SpTree b y c


-- 5.7 use splay heap to sort
spSort ::  Ord e => [e] -> [e]
spSort list = traverse (toheap list empty) []
    where toheap [] h                 = h
          toheap (e:t) h              = toheap t $ insert e h
          traverse Empty acc          = acc
          traverse (SpTree l x r) acc = traverse l $ x : traverse r acc


