{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Heap.BinomialHeap(BinoHeap, BinoHeap2, BinoHeap3) where

import Heap.Heap
import Control.Exception

--------------------------------------------------------------------------------
--  Binomial Heaps

data BinoTree a = BinoTree { rank :: Integer, node :: a, trees :: [BinoTree a]} deriving Show

binoSingleton ::  a -> BinoTree a
binoSingleton n = BinoTree 0 n []

_link ::  Ord a => BinoTree a -> BinoTree a -> BinoTree a
_link t1@(BinoTree r1 n1 ts1) t2@(BinoTree r2 n2 ts2) =
    assert (r1 == r2) (if n1 <= n2 then BinoTree (r1+1) n1 $ t2:ts1
                       else BinoTree (r1+1) n2 $ t1:ts2)

type BinoHeap a = [BinoTree a]

-- |this function is dangerous, one has to gurantee the first tree in heap has more rank 
-- or the same rank as t
_insertTree ::  Ord a => BinoTree a -> BinoHeap a -> BinoHeap a
_insertTree t [] = [t]
_insertTree t h@(th:ts)
    | rank t < rank th = t:h
    | otherwise        = _insertTree (_link t th) ts

binoInsert ::  Ord a => a -> BinoHeap a -> BinoHeap a
binoInsert n h = _insertTree (binoSingleton n) h

binoMerge ::  Ord a => BinoHeap a -> BinoHeap a -> BinoHeap a
binoMerge [] h = h
binoMerge h [] = h
binoMerge h1@(ht1:hs1) h2@(ht2:hs2)
    | rk1 == rk2 = _insertTree (_link ht1 ht2) $ binoMerge hs1 hs2
    | rk1 < rk2  = ht1 : binoMerge hs1 h2
    | otherwise  = ht2 : binoMerge hs2 h1
    where (rk1, rk2) = (rank ht1, rank ht2)

rmMinTree ::  Ord a => BinoHeap a -> (BinoTree a, BinoHeap a)
rmMinTree [t] = (t, [])
rmMinTree (t:ts)
    | node t <= node tmin = (t, ts)
    | otherwise           = (tmin, t:trem)
    where (tmin, trem) = rmMinTree ts

binoFindMin :: Ord a => BinoHeap a -> a
binoFindMin = node . fst . rmMinTree

binoDeleteMin :: Ord a => BinoHeap a -> BinoHeap a
binoDeleteMin h = binoMerge (reverse ts1) ts2
    where (BinoTree _ _ ts1, ts2) = rmMinTree h


instance (Ord a) => Heap (BinoHeap a) a where
    empty        = []
    isEmpty []   = True
    isEmpty _    = False
    insert       = binoInsert
    merge        = binoMerge
    findMin []   = Nothing
    findMin h    = Just $ binoFindMin h
    deleteMin [] = Nothing
    deleteMin h  = Just $ binoDeleteMin h


-- 3.6 get rid of redundant rank
data BinoTree2 a = BinoTree2 a [BinoTree2 a] deriving Show
type BinoHeap2 a = [(Integer, BinoTree2 a)]

binoSingleton2 ::  a -> (Integer, BinoTree2 a)
binoSingleton2 n = (0, BinoTree2 n [])

_link2 ::  Ord a => BinoTree2 a -> BinoTree2 a -> BinoTree2 a
_link2 t1@(BinoTree2 n1 ts1) t2@(BinoTree2 n2 ts2)
    | n1 <= n2  = BinoTree2 n1 $ t2:ts1
    | otherwise = BinoTree2 n2 $ t1:ts2

_insertTree2 :: Ord a => (Integer, BinoTree2 a) -> BinoHeap2 a -> BinoHeap2 a
_insertTree2 rt [] = [rt]
_insertTree2 rt@(r, t) h@((r2, t2):ts)
    | r < r2    = rt:h
    | otherwise = _insertTree2 (r+1, (_link2 t t2)) ts

binoInsert2 ::  Ord a => a -> BinoHeap2 a -> BinoHeap2 a
binoInsert2 n h = _insertTree2 (binoSingleton2 n) h

binoMerge2 :: Ord a =>BinoHeap2 a-> BinoHeap2 a -> BinoHeap2 a
binoMerge2 [] h = h
binoMerge2 h [] = h
binoMerge2 h1@(rt1@(r1, t1):ts1) h2@(rt2@(r2, t2):ts2)
    | r1 == r2  = _insertTree2 (r1+1, _link2 t1 t2) $ binoMerge2 ts1 ts2
    | r1 < r2   = rt1:binoMerge2 ts1 h2
    | otherwise = rt2:binoMerge2 ts2 h1

rmMinTree2 :: Ord a =>BinoHeap2 a -> ((Integer, BinoTree2 a), BinoHeap2 a)
rmMinTree2 [t]  = (t, [])
rmMinTree2 (t@(_, BinoTree2 nt _):ts)
    | nt < n    = (t, ts)
    | otherwise = (tmin, t:tres)
    where (tmin@(_, BinoTree2 n _), tres) = rmMinTree2 ts

-- a helper to generate infinite nonnegatives
nonneg ::  [Integer]
nonneg = [0..]

instance (Ord a) => Heap (BinoHeap2 a) a where
    empty        = []
    isEmpty []   = True
    isEmpty _    = False
    insert       = binoInsert2
    merge        = binoMerge2
    findMin []   = Nothing
    findMin h    = Just $ let ((_, BinoTree2 n _), _) = rmMinTree2 h in n
    deleteMin [] = Nothing
    deleteMin h  = Just $ let ((_, BinoTree2 _ ts), hts) = rmMinTree2 h in merge hts $ zip nonneg $ reverse ts


-- 3.7 constant time findMin
data BinoHeap3 a = Empty | BinoHeap3 a [(Integer, BinoTree2 a)] deriving Show

_insertTree3 :: Ord a => (Integer, BinoTree2 a) -> BinoHeap3 a -> BinoHeap3 a
_insertTree3 rt@(_, BinoTree2 n _) Empty = BinoHeap3 n [rt]
_insertTree3 rt@(_, (BinoTree2 n _)) (BinoHeap3 mini hl) =
    BinoHeap3 (min n mini) $ _insertTree2 rt hl


instance (Ord a) => Heap (BinoHeap3 a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    insert n h    = _insertTree3 (binoSingleton2 n) h
    merge Empty h = h
    merge h Empty = h
    merge (BinoHeap3 min1 hl1@(rt1@(r1, t1):ts1)) (BinoHeap3 min2 hl2@(rt2@(r2, t2):ts2))
        | r1 == r2  = BinoHeap3 rmin $_insertTree2 (r1+1, _link2 t1 t2) $ binoMerge2 ts1 ts2
        | r1 < r2   = BinoHeap3 rmin (rt1:binoMerge2 ts1 hl2)
        | otherwise = BinoHeap3 rmin (rt2:binoMerge2 ts2 hl1)
        where rmin  = min min1 min2

    findMin Empty               = Nothing
    findMin (BinoHeap3 m _)     = Just m
    deleteMin Empty             = Nothing
    deleteMin (BinoHeap3 _ hts) =
        Just $ case rmMinTree2 hts of 
            (_, [])                     -> Empty
            ((_, BinoTree2 _ ts), hts') -> -- used BinoHeap2 instance
                let (Just m1, Just m2) = (findMin tsheap2, findMin hts')
                    tsheap2            = zip nonneg $ reverse ts
                in BinoHeap3 (min m1 m2) $ merge tsheap2 hts'

