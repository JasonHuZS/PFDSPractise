{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Heap.HBLT(HBLT, fromList) where

import Heap.Heap

data HBLT a = Empty | Tree { rank  :: Integer
                           , node  :: a
                           , left  :: HBLT a
                           , right :: HBLT a } deriving Show


ltSingleton :: Ord a => a -> HBLT a
ltSingleton n = Tree 1 n Empty Empty


ltMerge ::  Ord a => HBLT a -> HBLT a -> HBLT a
ltMerge Empty t = t
ltMerge t Empty = t
ltMerge t1@(Tree{ node = n1 }) t2@(Tree{ node = n2 })
    | n1 < n2   = _ltMerge t1 t2
    | otherwise = _ltMerge t2 t1
    where _ltMerge tt1@Tree{ right = r1 } tt2 = 
            case tt1 of 
                (Tree _ tn1 Empty _) -> Tree 1 tn1 rspine Empty
                (Tree _ tn1 l1@Tree{ rank = rkl } _)
                    | rk <= rkl -> Tree (rk + 1)  tn1 l1 rspine
                    | otherwise -> Tree (rkl + 1) tn1 rspine l1
            where rspine@(Tree{ rank = rk }) = ltMerge r1 tt2


instance (Ord a) => Heap (HBLT a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    insert e h    = merge (ltSingleton e) h
    merge         = ltMerge

    findMin Empty              = Nothing
    findMin (Tree{ node = n }) = Just n

    deleteMin Empty                         = Nothing
    deleteMin (Tree{ left = l, right = r }) = Just $ merge l r


-- 3.2 defining insert direct won't make huge difference
-- 3.3 fromList merge two by two. each takes lgn time, adds up n time
fromList ::  (Ord e) => [e] -> HBLT e
fromList list = case iter . fromHeaps $ toSingleton list of 
                    []  -> Empty
                    [h] -> h
    where toSingleton          = map ltSingleton
          fromHeaps l@[]       = l
          fromHeaps l@[_]      = l
          fromHeaps (h1:h2:hs) = merge h1 h2 : fromHeaps hs
          iter l@(_:_:_)       = iter $ fromHeaps l
          iter l               = l


