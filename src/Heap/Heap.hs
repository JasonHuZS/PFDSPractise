{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances #-}

module Heap.Heap where

class (Ord e) => Heap h e | h -> e where
    empty     :: h
    isEmpty   :: h -> Bool
    insert    :: e -> h -> h
    merge     :: h -> h -> h
    findMin   :: h -> Maybe e
    deleteMin :: h -> Maybe h

