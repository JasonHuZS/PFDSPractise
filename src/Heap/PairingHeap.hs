{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ViewPatterns #-}

module Heap.PairingHeap(PHeap, Tree, toBinary) where

import Heap.Heap


-- the list in PTree shall contain no PEmpty. so use type system to enforce that
newtype PTree a = PTree (a, [PTree a])  deriving Show
data    PHeap a = PEmpty | PT (PTree a) deriving Show


instance Ord a => Heap (PHeap a) a where
    empty         = PEmpty
    isEmpty PEmpty = True
    isEmpty _     = False
    insert e h    = merge (PT $ PTree (e, [])) h

    merge PEmpty h = h
    merge h PEmpty = h
    merge (PT h1@(PTree (e1, hs1))) (PT h2@(PTree (e2, hs2)))
        | e1 < e2   = PT $ PTree (e1, h2:hs1)
        | otherwise = PT $ PTree (e2, h1:hs2)

    findMin PEmpty                  = Nothing
    findMin (PT (PTree (m, _)))    = Just m
    deleteMin PEmpty                = Nothing
    deleteMin (PT (PTree (_, hs))) = Just $ mergePairs hs
        where mergePairs []          = PEmpty
              mergePairs [h]         = PT h
              mergePairs (h1:h2:hs') = merge (merge (PT h1) $ PT h2) $ mergePairs hs'

data Tree a = Empty | Tree (Tree a) a (Tree a) deriving Show

-- |convert pairing heap to binary tree.
-- LCRS transform seems to be problematic. the resulting tree is not a BST.
toBinary ::  PHeap a -> Tree a
toBinary PEmpty   = Empty
toBinary (PT hp) = _toBin hp []
    where toBin []                    = Empty
          toBin (h:t)                 = _toBin h t
          _toBin (PTree (x, hs)) rsib = Tree (toBin hs) x (toBin rsib)

