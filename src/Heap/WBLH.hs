{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Heap.WBLH(WBLH) where

import Heap.Heap

data WBLH a = Empty | Tree { weight :: Integer
                           , node   :: a
                           , left   :: WBLH a
                           , right  :: WBLH a } deriving Show


wblhMerge ::  Ord a => WBLH a -> WBLH a -> WBLH a
wblhMerge Empty t = t
wblhMerge t Empty = t
wblhMerge t1@(Tree{ node = n1 }) t2@(Tree{ node = n2 })
    | n1 < n2   = _merge t1 t2
    | otherwise = _merge t2 t1
    where _merge tt1@Tree{ right = r1 } tt2 = 
            case tt1 of
                (Tree _ tn1 Empty _) -> Tree (w+1) tn1 rspine Empty
                (Tree _ tn1 l1@Tree{ weight = w1 } _)
                    | w <= w1   -> Tree (w+w1+1) tn1 l1 rspine
                    | otherwise -> Tree (w+w1+1) tn1 rspine l1
            where rspine@(Tree{ weight = w }) = wblhMerge r1 tt2

-- topdown one-pass version of merge for WBLH
tdWBLHMerge :: Ord a => WBLH a -> WBLH a -> WBLH a
tdWBLHMerge Empty t = t
tdWBLHMerge t Empty = t
tdWBLHMerge t1@(Tree{ node = n1 }) t2@(Tree{ node = n2 })
    | n1 < n2   = _merge t1 t2
    | otherwise = _merge t2 t1
    where _merge (Tree _ tn1 Empty _) tt2   = Tree (1 + weight tt2) tn1 tt2 Empty
          _merge (Tree w1 tn1 l1 r1) tt2
            | wtt2 + weight r1 <= weight l1 = Tree (w1+wtt2) tn1 l1 $ tdWBLHMerge r1 tt2
            | otherwise                     = Tree (w1+wtt2) tn1 (tdWBLHMerge r1 tt2) $ l1
            where wtt2 = weight tt2


instance Ord a => Heap (WBLH a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    insert e h    = merge h $ Tree 1 e empty empty
    merge         = tdWBLHMerge

    findMin Empty              = Nothing
    findMin (Tree{ node = n }) = Just n

    deleteMin Empty                         = Nothing
    deleteMin (Tree{ left = l, right = r }) = Just $ merge l r
