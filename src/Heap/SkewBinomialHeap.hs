{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Heap.SkewBinomialHeap where

import Heap.Heap
import Data.List(foldl')

{- 
   using Skew representation with original binomial heap 
   to achieve O(1) worst case insertion
-}

data Tree a = Tree{ rank  :: Integer
                  , node  :: a
                  , elems :: [a]
                  , trees :: [Tree a] } deriving Show

newtype SBHeap a = SBHeap [Tree a] deriving Show

pattern Empty = SBHeap []


link ::  Ord a => Tree a -> Tree a -> Tree a
link t1 t2
    | x1 < x2   = t1{rank = r + 1, trees = t2:trees t1}
    | otherwise = t2{rank = r + 1, trees = t1:trees t2}
    where r     = rank t1
          x1    = node t1
          x2    = node t2


skewLink ::  Ord a => a -> Tree a -> Tree a -> Tree a
skewLink e t1 t2
    | e < node       = t{node = e, elems = node:elems}
    | otherwise      = t{elems = e:elems}
    where t@Tree{..} = link t1 t2


insTree ::  Ord a => Tree a -> [Tree a] -> [Tree a]
insTree t [] = [t]
insTree t (ht:ts)
    | rank t < rank ht = t:ht:ts
    | otherwise        = link t ht:ts


mergeTrees ::  Ord a => [Tree a] -> [Tree a] -> [Tree a]
mergeTrees ts [] = ts
mergeTrees [] ts = ts
mergeTrees (t1:ts1) (t2:ts2)
    | r1 < r2   = (t1:) $ mergeTrees ts1 $ t2:ts2
    | r1 > r2   = t2:mergeTrees (t1:ts1) ts2
    | otherwise = insTree (link t1 t2) $ mergeTrees ts1 ts2
    where r1    = rank t1
          r2    = rank t2


rmMinTree ::  Ord a => [Tree a] -> (Tree a, [Tree a])
rmMinTree [t] = (t, [])
rmMinTree (h:ts@(rmMinTree -> (tm, trem)))
    | node h < node tm = (h, ts)
    | otherwise        = (tm, h:trem)


instance (Ord a) => Heap (SBHeap a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    insert e (SBHeap (t1:t2:ts))
        | rank t1 == rank t2 = SBHeap $ skewLink e t1 t2:ts
    insert e (SBHeap ts)     = SBHeap $ Tree 0 e [] []:ts
    merge (SBHeap ts1) (SBHeap ts2) = SBHeap $ mergeTrees (normalize ts1) $ normalize ts2
        where -- normalize eliminate possibly existing first two trees with the same rank in skew number construct
              normalize []    = []
              normalize (h:t) = insTree h t

    findMin Empty       = Nothing
    findMin (SBHeap ts) = Just $ node $ fst $ rmMinTree ts

    deleteMin Empty       = Nothing
    deleteMin (SBHeap ts) = Just $ insertAll elems $ merge (SBHeap $ reverse trees) $ SBHeap ts'
        where (Tree{..}, ts') = rmMinTree ts
              insertAll       = flip $ foldl' (flip insert)
