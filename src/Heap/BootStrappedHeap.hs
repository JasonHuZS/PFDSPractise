{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}

module Heap.BootStrappedHeap where

import Heap.Heap

{-
given a primitive heap with O(1) worst case insertion, 
O(1) worst case findMin and merge can be achieved.
-}

data BSHeap h a = Empty
                | BSHeap{ node :: a
                        , heap :: h (BSHeap h a) }


instance (Show a, Show (h (BSHeap h a))) => Show (BSHeap h a) where
    show Empty      = "Empty"
    show BSHeap{..} = "BSHeap{node = " ++ show node ++ ", heap = " ++ show heap ++ "}"


compareHeap :: (Ord a, Heap h a) => h -> h -> Ordering
compareHeap h1 h2
    | isEmpty h1
    , isEmpty h2 = EQ
    | isEmpty h1 = LT
    | isEmpty h2 = GT
    | otherwise  = case compare hp1 hp2 of
                     EQ -> compareHeap h1' h2'
                     r  -> r
        where Just hp1 = findMin h1
              Just hp2 = findMin h2
              Just h1' = deleteMin h1
              Just h2' = deleteMin h2


instance (Ord a, Heap (h (BSHeap h a)) (BSHeap h a)) => Eq (BSHeap h a) where
    h1 == h2 = EQ == compare h1 h2


instance (Ord a, Heap (h (BSHeap h a)) (BSHeap h a)) => Ord (BSHeap h a) where
    compare Empty Empty = EQ
    compare Empty h = LT
    compare h Empty = GT
    compare h1 h2 = case compare (node h1) $ node h2 of
        EQ -> compareHeap (heap h1) $ heap h2
        r  -> r


instance (Ord a, Heap (h (BSHeap h a)) (BSHeap h a)) => Heap (BSHeap h a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    insert e h    = merge (BSHeap e empty) h
    merge Empty h = h
    merge h Empty = h
    merge hp1@(BSHeap n1 h1) hp2@(BSHeap n2 h2)
        | n1 < n2        = hp1{heap = insert hp2 h1}
        | otherwise      = hp2{heap = insert hp1 h2}
    findMin Empty        = Nothing
    findMin BSHeap{node} = Just node
    deleteMin Empty      = Nothing
    deleteMin BSHeap{heap}
        | isEmpty heap = Just Empty
        | otherwise    = Just $ BSHeap node $ merge hp1 hp2
            where Just BSHeap{node, heap = hp1} = findMin heap
                  Just hp2                      = deleteMin heap
