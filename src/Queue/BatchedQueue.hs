{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies, FlexibleInstances #-}

module Queue.BatchedQueue(BQ) where

import Queue.Queue 


-- |a batched queue is represented as two lists, elements in one go in correct order,
-- while elements in the other one go the opposite.
newtype BQ a = BQ ([a], [a]) deriving Show

-- |guarantee batched queue invariance
-- when the first list is empty, the second one must be so too.
_bqinvariance ::  BQ a -> BQ a
_bqinvariance (BQ ([], r@(_:_))) = BQ (reverse r, [])
_bqinvariance q                  = q

instance Queue (BQ a) a where
    empty                 = BQ ([], [])
    isEmpty (BQ ([], [])) = True
    isEmpty _             = False
    head (BQ ([], _))     = Nothing
    head (BQ ((h:_), _))  = Just h
    tail (BQ ([], _))     = Nothing
    tail (BQ ((_:t), r))  = Just $ _bqinvariance $ BQ (t, r)
    snoc (BQ (f, r)) e    = _bqinvariance $ BQ (f, e:r)


