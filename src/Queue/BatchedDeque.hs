{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

module Queue.BatchedDeque(BD) where

import Queue.Queue
import Queue.Deque

-- 5.1 extend Batched Queue to implement Deque
-- using potential function OMEGA(BD (f, r)) = abs(|f| - |r|) can prove this deque is
-- amortized constant time.
-- amortized cost is 1.5 usual cost(getting first elem out of list).
-- so amortizedly deque is cheaper than queue while shipped with more functionality,
-- however, invariance maintenance happens more frequently(roughly twice), such that
-- the performance of deque could be unstable.

newtype BD a = BD ([a], [a]) deriving Show

-- |guarantee invariance of bathced deque
-- two lists can't be empty if elements are sufficient (>=2)
_bdinvariance ::  BD t -> BD t
_bdinvariance q@(BD ([], []))  = q
_bdinvariance q@(BD ([], [_])) = q
_bdinvariance q@(BD ([_], [])) = q
_bdinvariance (BD ([], l))     = let (r, rf) = splitAt ((1 + length l) `div` 2) l
                                 in BD (reverse rf, r)
_bdinvariance (BD (l, []))     = let (f, rr) = splitAt ((1 + length l) `div` 2) l 
                                 in BD (f, reverse rr)
_bdinvariance q                = q


instance Queue (BD a) a where
    empty                 = BD ([], [])
    isEmpty (BD ([], [])) = True
    isEmpty _             = False

    head (BD ([], []))    = Nothing
    head (BD ([], [h]))   = Just h
    head (BD (h:_, _))    = Just h
    tail (BD ([], []))    = Nothing
    tail (BD ([], [_]))   = Just empty
    tail (BD (_:t, r))    = Just $ _bdinvariance $ BD (t, r)
    snoc (BD (f, r)) e    = _bdinvariance $ BD (f, e:r)


instance Deque (BD a) a where
    last (BD ([], []))    = Nothing
    last (BD ([l], []))   = Just l
    last (BD (_, l:_))    = Just l
    init (BD ([], []))    = Nothing
    init (BD ([_], []))   = Just empty
    init (BD (f, _:t))    = Just $ _bdinvariance $ BD (f, t)
    cons e (BD (f, r))    = _bdinvariance $ BD (e:f, r)

