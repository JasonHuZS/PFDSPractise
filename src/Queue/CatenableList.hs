{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Queue.CatenableList(CatenableList(..), CatList) where
    
import Queue.Queue
import Prelude hiding (head, tail, (++))
import qualified Prelude


class Queue l a => CatenableList l a | l -> a where
    cons :: a -> l -> l
    (++) :: l -> l -> l


data CatList q a = Empty
                 | CL{ node  :: a
                     , queue :: q (CatList q a) }


instance (Show a, Show (q (CatList q a))) => Show (CatList q a) where
    show Empty = "Empty"
    show CL{..} = "CL{node = " Prelude.++ show node Prelude.++ ", queue = " Prelude.++ show queue Prelude.++ "}"


link :: Queue (q (CatList q a)) (CatList q a) => CatList q a -> CatList q a -> CatList q a
link CL{..} q = CL node $ snoc queue q


linkall :: Queue (q1 (CatList q1 a)) (CatList q1 a) =>q1 (CatList q1 a) -> CatList q1 a
linkall q = let Just h = head q
                Just t = tail q
             in if isEmpty t then h else link h $ linkall t


instance Queue (q (CatList q a)) (CatList q a) => Queue (CatList q a) a where
    empty         = Empty
    isEmpty Empty = True
    isEmpty _     = False
    snoc l x      = l ++ CL x empty
    head Empty    = Nothing
    head CL{node} = Just node
    tail Empty    = Nothing
    tail CL{queue}
        | isEmpty queue = Just Empty
        | otherwise     = Just $ linkall queue


instance Queue (q (CatList q a)) (CatList q a) => CatenableList (CatList q a) a where
    cons x l   = CL x empty ++ l
    l ++ Empty = l
    Empty ++ l = l
    l1 ++ l2   = link l1 l2


flatten :: CatenableList l a => [l] -> l
flatten = foldr (\q1 q2 -> q1 ++ q2) empty
