{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Queue.Deque where

import Queue.Queue

class Queue d e => Deque d e | d -> e where
    cons    :: e -> d -> d
    last    :: d -> Maybe e
    init    :: d -> Maybe d

