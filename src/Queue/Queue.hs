{-# LANGUAGE MultiParamTypeClasses, FunctionalDependencies #-}

module Queue.Queue where

class Queue q e | q -> e where
    empty   :: q
    isEmpty :: q -> Bool
    snoc    :: q -> e -> q
    head    :: q -> Maybe e
    tail    :: q -> Maybe q
